<?php

namespace Triangl\Tests;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Triangl\WebTestCase;
use Triangl\Application;

/**
 * Functional test for Triangl Web Services library.
 */
class WebServicesTest extends WebTestCase {
    /**
     * Implemented.
     */
    public function createApplication() {
        return new Application( 
            __DIR__ . "/../../../var", 
            array("test" => true) 
        );
    }
    
    /**
     * Tests dynamic application route.
     */
    public function testDynamicRoute() {
        $app = $this->app;
        $messages = array(
            1 => array(
                'title'     => 'Using Silex',
                'body'      => 'Using Silex is fun!',
            ),
            2 => array(
                'title'     => 'Using PHPUnit',
                'body'      => 'Using PHPUnit is fun too!',
            )
        );
        
        $app->get('/message/{id}', function (Application $app, $id) use ($messages) {
            if (!isset($messages[$id])) {
                $app->abort(404, "Message $id does not exist.");
            }
            
            $item = $messages[$id];            
            
            return new Response("<h1>{$item['title']}</h1>".
                "<p>{$item['body']}</p>");
        })
        ->assert('id', '\d+');
        
        $client = $this->createClient();
        $crawler = $client->request('GET', '/message/1');        
        $this->assertTrue( $client->getResponse()->isOk() );
        $this->assertCount(1, $crawler->filter('h1'));
        $this->assertCount(1, $crawler->filter('p'));
    }
    
    /**
     * Tests session storage functionality.
     */
    public function testSessionStorage() {
        $app = $this->app;
        $param = "Session test.";
                
        $app->post('/session', function (Application $app, Request $request) {
            $bar = $request->get('param');
            $app['session']->set('param', $bar);
            return new Response('Session param was set.', 201);
        });
        $app->get('/session', function (Application $app) use ($param) {
            $result = $app['session']->get('param');
            if ($result !== $param) {
                $app->abort(500, "Test failed.");
            }
            return $result;
        });
        
        $client = $this->createClient();
        $client->request( 'POST', '/session', array("param" => $param) );
        $this->assertTrue( $client->getResponse()->getStatusCode() == 201 );
        $client->request( 'GET', '/session');
        $this->assertTrue( $client->getResponse()->isOk() );
    }
}
