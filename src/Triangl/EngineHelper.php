<?php

namespace Triangl;

/**
 * Helper class for 3angl engine.
 */
class EngineHelper {
    /**
     * Call this to check if specifed path is valid.
     * @param string $path
     * @param bool $checkWriteability
     * @throws \RuntimeException
     */
    public function checkPath($path, $checkWriteability = false) {
        if ( !file_exists($path) ) {
            throw new \RuntimeException("Path $path is invalid.");
        }
        if ( $checkWriteability && !is_writable($path) ) {
            throw new \RuntimeException("Path $path is not writeable.");
        }
    }
    
    /**
     * Creates target file if not present.
     * @param string $path
     */
    public function createFileIfNotExist($path) {
        if ( !file_exists($path) ) {
            file_put_contents($path, '');
        }
    }
    
    /**
     * Creates target dir if not present.
     * @param string $path
     */
    public function createDirIfNotExist($path) {
        if ( !file_exists($path) ) {
            mkdir($path, 0777, true);
        }
    }
}
