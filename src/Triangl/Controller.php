<?php

namespace Triangl;

/**
 * Defines a controller.
 */
class Controller {
    protected $app;
    
    /**
     * Default constructor.
     */
    public function __construct(Application $app) {
        $this->app = $app;
    }
}
