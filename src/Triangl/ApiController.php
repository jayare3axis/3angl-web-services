<?php

namespace Triangl;

/**
 * Defines a controller used for api calls.
 */
class ApiController extends Controller {
    /**
     * Formats exception for client.
     * @param \Exception $e
     */
    protected function formatException(\Exception $e) {
        return array(
            'error' => $e->getMessage()
        );
    }
}
