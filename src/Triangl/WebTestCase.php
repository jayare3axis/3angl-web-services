<?php

namespace Triangl;

/**
 * For functional tests purpose.
 */
abstract class WebTestCase extends \Silex\WebTestCase {    
    /**
     * Executes simple test adding ping route and checking response.
     */
    public function testPing() {
        $app = $this->app;
        
        $app->get('/ping', function () use ($app) {
            return "Hello world.";
        });
        
        $this->checkRequest('/ping');
    }
    
    /**
     * Creates request on given url address and checks that response is OK.
     * @param string $url url address
     * @param string $method optional
     */
    protected function checkRequest($url, $method = 'GET') {
        $client = $this->createClient();
        $client->request($method, $url);        
        $this->assertTrue( $client->getResponse()->isOk() );
    }
}
