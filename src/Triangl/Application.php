<?php

namespace Triangl;

use Silex\Application\MonologTrait;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

/**
 * Base class of all 3angl Web Services.
 */
class Application extends \Silex\Application {
    use MonologTrait;
    
    /**
     * Default constructor.
     * For testing environments make sure that "test" => true is contained within values parameter.
     * @param string $path path to application var folder
     * @param array $values
     */
    public function __construct( $path, array $values = array() ) {
        parent::__construct($values);
        
        $this["path"] = $path;                 
        if ( !array_key_exists("test", $values) ) {
            $this["test"] = false;
        }        
        $this["debug"] = $this->isDebug();
        
        //$this[locale"] = "cz";
        $this->init();
    }
    
    /**
     * Determine if application is in debug mode.
     * Default implementation determines debug mode for localhost environment
     * or for testing environments.
     * @return boolean
     */
    public function isDebug() {
        $result = ($this["test"] || $_SERVER["REMOTE_ADDR"] === "127.0.0.1");
        return $result;
    }
    
    /**
     * Callback invoked when application invokes unhandled exception.
     * Return message to be displayed to client.
     * This is triggered only if debug mode is on.
     * @param \Exception $e
     * @param int $code
     * @return string
     */
    protected function onException(\Exception $e, $code) {        
        switch ($code) {
            case 404:
                $message = 'The requested page could not be found.';
                break;
            default:
                $message = 'We are sorry, but something went terribly wrong.';
        }
        return $message;
    }
    
    /**
     * Format error message for client.
     * This is triggered only if debug mode is on.
     * @param string $message
     * @return Symfony\Component\HttpFoundation\Response
     */
    protected function getErrorAsResponse($message) {
        return new Response($message);
    }
    
    /**
     * Application initialization routine.
     * Override to register services.
     */
    protected function init() {
        // Prepare testing environment.
        if ($this["test"]) {
            $this['exception_handler']->disable();            
        }
        else {       
            // Allow use of put and delete etc.    
            Request::enableHttpMethodParameterOverride();
            // Convert PHP errors to exceptions.
            ErrorHandler::register();
            // Covert PHP fatal errors as well.    
            ExceptionHandler::register();
        }
        
        // Report exceptions in production mode.
        if ( !$this->isDebug() ) {
            $this->error(function (\Exception $e, $code) {
                $message = $this->onException($e, $code);
                return $this->getErrorAsResponse($message);
            });
        }
        
        $this['triangl.engine'] = $this->share( function ($app) {
            return new EngineHelper($app);
        } );
        
        
        $this->register( new Engine() );
    }
}
