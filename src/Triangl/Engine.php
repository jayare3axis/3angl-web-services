<?php

namespace Triangl;

use Symfony\Component\HttpFoundation\Request;

use Silex\ServiceProviderInterface;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;

/*
 * Triangl core module.
 */
class Engine implements ServiceProviderInterface {
    /**
     * Implemented.
     */
    public function boot(\Silex\Application $app) {        
    }

    /**
     * Implemented.
     */
    public function register(\Silex\Application $app) {
        // Default configurations.
        $app['db'] = array(
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'dbname' => 'triangl',
            'user' => 'root',
            'password' => null,
        );
        $app["monolog.values"] = array(
            'monolog.logfile' => $this->getLogFilePath($app)
        );
        $app["session.values"] = array(
            'session.storage.save_path' => $app["path"] . '/sessions',
            'session.test' => $app["test"]
        );
        $app["session.autostart"] = true;
        
        $engine = $app["triangl.engine"];        
        $engine->createDirIfNotExist($app["path"]);
        $engine->createDirIfNotExist($app["path"] . "/log");
        $engine->createDirIfNotExist($app["path"] . "/sessions");
        $engine->createFileIfNotExist( $this->getLogFilePath($app) );
        
        // Register services.        
        $this->registerLogService($app, $app["monolog.values"]);        
        $this->registerSessionService($app, $app["session.values"]);
        $app->register( new ServiceControllerServiceProvider() );
        
        // If debugger is on check if paths provided are valid.
        if ( $app->isDebug() ) {
            $engine->checkPath($app["path"]);
            $engine->checkPath($this->getLogFilePath($app), true);
        }      
        
        // Automatically start session if necessary.
        if ($app["session.autostart"]) {
            $app->before(function (Request $request) {
                $request->getSession()->start();
            });
        }
    }
    
    /**
     * Register service used for logging.
     * Default implementation uses Monolog provider.
     * @param Triangl\Core\Application $app application instance.
     * @param array $options parameters
     */
    protected function registerLogService(Application &$app, array $options) {
        $app->register( new MonologServiceProvider(), $options );
    }
    
    /**
     * Register service used for sessions.
     * @param Triangl\Core\Application $app application instance.
     */
    protected function registerSessionService(Application &$app, array $options) {
        $app->register( new SessionServiceProvider(), $options );
    }
    
    /**
     * Gets name of log file.
     * Defaults to debug.log or production.log based on debug mode.
     * @param Triangl\Core\Application $app application instance.
     */
    protected function getLogFileName(Application $app) {
        if ( $app["test"] === true ) {
            return "test.log";
        }
        else if ( $app->isDebug() ) {
            return 'debug.log';
        }
        else {
            return 'production.log';
        }
    }
    
    /**
     * @ignore
     */
    private function getLogFilePath(Application $app) {        
        return $app["path"] . "/log/" . $this->getLogFileName($app);
    }
}
